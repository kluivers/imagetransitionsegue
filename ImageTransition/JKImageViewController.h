//
//  JKImageViewController.h
//  ImageTransition
//
//  Created by Joris Kluivers on 1/12/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKImageViewController : UIViewController <UIScrollViewDelegate>

@property(strong) UIImage *image;

@property(weak) IBOutlet UIScrollView *scrollView;
@property(weak) IBOutlet UIImageView *imageView;

@end
