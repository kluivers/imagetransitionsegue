//
//  JKViewController.h
//  ImageTransition
//
//  Created by Joris Kluivers on 1/12/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKViewController : UIViewController

@property(nonatomic, weak) IBOutlet UIButton *imageButton;

- (IBAction) unwindFromSegue:(UIStoryboardSegue *)segue;

@end
