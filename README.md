# Custom UIStoryboardSegue

A view controller transition that animates an image from the originating view controller to the new view controller. This imitates the animation you see in the Facebook iOS app when tapping an image in your feed. Implemented as a `UIStoryboardSegue`.

Demo video:
[http://www.youtube.com/watch?v=7koIjOMKaEk][tube]

by **Joris Kluivers**

- Blog post: [Custom view controller transitions using UIStoryboardSegue](http://joris.kluivers.nl/blog/2013/01/15/custom-view-controller-transitions-using-uistoryboardsegues/)
- Follow [@kluivers on Twitter](http://twitter.com/kluivers)

## Screenshot

Showing an unwind transition example.

![Transition screenshot][screenshot]

1. release scroll position
2. Image animation & controller cross fade
3. Back in it's original position

## License

Copyright (c) 2012, Joris Kluivers
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 - Redistributions of source code must retain the above copyright 
   notice, this list of conditions and the following disclaimer.
 - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 - Neither the name of the author nor the
   names of any contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

[screenshot]: https://bitbucket.org/kluivers/imagetransitionsegue/raw/4b3b6d64d3800c377e783079cc598e2b21473a67/Screenshot.png
[tube]: http://www.youtube.com/watch?v=7koIjOMKaEk
